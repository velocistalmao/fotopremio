package com.example.jorge.fotopremio.test;

import android.content.pm.PackageManager;
import android.test.AndroidTestCase;

/**
 * Test para permisos.
 */
public class PermissionsTest extends AndroidTestCase {

    private PackageManager packageManager;
    private int request;

    protected void setUp(){
        packageManager = this.getContext().getPackageManager();
    }

    protected void tearDown() {
        packageManager = null;
    }

    public void test_permission_INTERNET() {
        request = packageManager.checkPermission("android.permission.INTERNET","com.example.jorge.fotopremio");
        assertEquals(request, PackageManager.PERMISSION_GRANTED );
    }

    public void test_permission_CAMERA() {
        request = packageManager.checkPermission("android.permission.CAMERA","com.example.jorge.fotopremio");
        assertEquals(request, PackageManager.PERMISSION_GRANTED );
    }

    public void test_permission_WRITE_EXTERNAL_STORAGE() {
        request = packageManager.checkPermission("android.permission.WRITE_EXTERNAL_STORAGE","com.example.jorge.fotopremio");
        assertEquals(request, PackageManager.PERMISSION_GRANTED );
    }

}
