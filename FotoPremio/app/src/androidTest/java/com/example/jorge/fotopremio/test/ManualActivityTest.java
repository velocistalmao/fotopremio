package com.example.jorge.fotopremio.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.test.ViewAsserts;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.gui.view.ManualActivity;

/**
 * Created by Jorge on 12/05/2015.
 */
public class ManualActivityTest extends ActivityInstrumentationTestCase2<ManualActivity> {

    public ManualActivityTest() {
        super(ManualActivity.class);
    }

    public void test_exists_items() {
        ManualActivity manualActivity = this.getActivity();
        TextView textView = (TextView) manualActivity.findViewById(R.id.txt_manual_numero);
        ViewAsserts.assertOnScreen(manualActivity.getWindow().getDecorView(), textView);
        textView = (TextView) manualActivity.findViewById(R.id.txt_manual_serie);
        ViewAsserts.assertOnScreen(manualActivity.getWindow().getDecorView(), textView);
        textView = (TextView) manualActivity.findViewById(R.id.txt_manual_raffle);
        ViewAsserts.assertOnScreen(manualActivity.getWindow().getDecorView(), textView);
        textView = (TextView) manualActivity.findViewById(R.id.txt_manual_fraccion);
        ViewAsserts.assertOnScreen(manualActivity.getWindow().getDecorView(), textView);

        EditText editText = (EditText) manualActivity.findViewById(R.id.ed_manual_numero);
        ViewAsserts.assertOnScreen(manualActivity.getWindow().getDecorView(), editText);
        editText = (EditText) manualActivity.findViewById(R.id.ed_manual_serie);
        ViewAsserts.assertOnScreen(manualActivity.getWindow().getDecorView(), editText);
        editText = (EditText) manualActivity.findViewById(R.id.ed_manual_fraccion);
        ViewAsserts.assertOnScreen(manualActivity.getWindow().getDecorView(), editText);
        editText = (EditText) manualActivity.findViewById(R.id.ed_manual_raffle);
        ViewAsserts.assertOnScreen(manualActivity.getWindow().getDecorView(), editText);


    }

    public void test_button() {
        ManualActivity manualActivity = this.getActivity();
        final Button button = (Button) manualActivity.findViewById(R.id.btn_manual_stock);

        TouchUtils.clickView(this, button);

        // button.performClick();
    }
}
