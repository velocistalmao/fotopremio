package com.example.jorge.fotopremio.test;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.base.ConnectionServer;
import com.example.jorge.fotopremio.base.JsonDataRaffle;
import com.example.jorge.fotopremio.base.JsonPrizesLotto;
import com.example.jorge.fotopremio.model.Decimo;

import junit.framework.TestCase;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by Jorge on 23/06/2015.
 */
public class JsonPrizesLottoTest extends TestCase {

    private JsonPrizesLotto jsonPrizesLotto;
    private ConnectionServer connectionServer;

    protected void setUp() throws IOException, JSONException {
        String url = "http://www.loteriasyapuestas.es/servicios/premioDecimoWeb?idsorteo=912109050";
        connectionServer =  new ConnectionServer();
        String request = connectionServer.connect(url);

        jsonPrizesLotto = new JsonPrizesLotto(request);
        jsonPrizesLotto.jsonDocCharge();
    }

    protected void tearDown() {
        jsonPrizesLotto = null;
        connectionServer = null;
    }

    public void test_getSpecialPrize(){

        Decimo decimo = new Decimo();
        decimo.setNumero("07011");
        decimo.setSerie("007");
        decimo.setFraccion("02");
        decimo.setNumero("6");
        decimo.setFecha("20-06-2015");


        try {

            assertTrue(jsonPrizesLotto.returnSpecialPrize().equals("3000000"));

            assertTrue(jsonPrizesLotto.numberCheck("21011").equals("96"));

        } catch (JSONException e) {
            assertNotNull(null);
        }


    }


}
