package com.example.jorge.fotopremio.test;

import com.example.jorge.fotopremio.base.ConnectionServer;
import com.example.jorge.fotopremio.base.JsonDataRaffle;

import junit.framework.TestCase;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

/**
 * Created by Jorge on 12/05/2015.
 */
public class JsonDataRaffleTest extends TestCase {

    private JsonDataRaffle jsonDataRaffle;
    private ConnectionServer connectionServer;

    protected void setUp() throws IOException, JSONException {
        String url = "http://www.alaejemplos.esy.es/dataraffle.json";
        connectionServer = new ConnectionServer();
        String request = connectionServer.connect(url);

        jsonDataRaffle = new JsonDataRaffle(request);
        jsonDataRaffle.jsonDocCharge();
    }

    protected void tearDown() {
        jsonDataRaffle = null;
        connectionServer = null;
    }

    public void test_getDataRaffle() throws JSONException {
        List<String> list = jsonDataRaffle.getDataRaffle("0375");

        assertEquals("07-05-2015", list.get(0));
        assertEquals("907709037", list.get(1));
        assertEquals("3", list.get(2));
    }

}
