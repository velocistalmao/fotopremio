package com.example.jorge.fotopremio.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.gui.view.LottoListActivity;
import com.example.jorge.fotopremio.gui.view.ManualActivity;

/**
 * Created by Jorge on 23/06/2015.
 */
public class LottoListActivityTest extends ActivityInstrumentationTestCase2<LottoListActivity> {

    public LottoListActivityTest() {
            super(LottoListActivity.class);
    }

    public void test_exists_items(){

        LottoListActivity lottoListActivity = this.getActivity();
        ListView listView = (ListView) lottoListActivity.findViewById(R.id.listView_lotto1);
        ViewAsserts.assertOnScreen(lottoListActivity.getWindow().getDecorView(), listView);

    }

}
