package com.example.jorge.fotopremio.test;

import com.example.jorge.fotopremio.base.ConnectionServer;

import junit.framework.TestCase;

import java.io.IOException;

/**
 * Test para la clase ConnectionServer.
 */
public class ConnectionServerTest extends TestCase {

    private ConnectionServer connectionServer;

    protected void setUp() {
        connectionServer = new ConnectionServer();
    }

    protected void tearDown() {
        connectionServer = null;
    }

    public void test_connect() throws IOException {
        String url = "http://www.alaejemplos.esy.es/dataraffle.json";
        String request = connectionServer.connect(url);
        assertNotNull(request);
    }
}
