package com.example.jorge.fotopremio.test;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.example.jorge.fotopremio.gui.view.MainActivity;
import com.example.jorge.fotopremio.manager.SQLManagement;
import com.example.jorge.fotopremio.model.Decimo;

/**
 * Test BBDD.
 */
public class SQLManagementTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private SQLManagement sqlManagement;

    public SQLManagementTest() {
        super(MainActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        sqlManagement = new SQLManagement(getActivity());
    }

    protected void tearDown() throws Exception {
        sqlManagement = null;
        super.tearDown();
    }

    public void test_BDOperations(){

        //add
        Decimo decimo1 = new Decimo();
        decimo1.setNumero("11111");
        decimo1.setFraccion("01");
        decimo1.setSerie("001");
        decimo1.setEuros("3");
        decimo1.setFecha("04-06-2015");

        sqlManagement.addDecimo(decimo1, 20150604);

        Decimo decimo2 = new Decimo();
        decimo2.setNumero("22222");
        decimo2.setFraccion("02");
        decimo2.setSerie("002");
        decimo2.setEuros("3");
        decimo2.setFecha("04-06-2015");

        sqlManagement.addDecimo(decimo2, 20150604);

        Decimo decimo3 = new Decimo();
        decimo3.setNumero("33333");
        decimo3.setFraccion("03");
        decimo3.setSerie("003");
        decimo3.setEuros("3");
        decimo3.setFecha("04-06-2015");

        sqlManagement.addDecimo(decimo3, 20150604);

        //getAll
        assertTrue(sqlManagement.getDecimosList().size() == 3);

        //get by id
        int id = sqlManagement.getIdDecimo(decimo3);
        //delete by id
        sqlManagement.deleteDecimo(id);

        assertTrue(sqlManagement.getDecimosList().size() == 2);

        //delete all
        sqlManagement.deleteAll();

        assertTrue(sqlManagement.getDecimosList().size() == 0);

    }


}
