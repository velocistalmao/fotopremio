package com.example.jorge.fotopremio.test;

import android.test.ActivityInstrumentationTestCase2;

import com.example.jorge.fotopremio.gui.view.MainActivity;
import com.example.jorge.fotopremio.gui.view.StartCameraActivity;
import com.example.jorge.fotopremio.manager.OCRManagement;
import com.example.jorge.fotopremio.manager.SQLManagement;
import com.example.jorge.fotopremio.model.LowConfidenceException;

import java.util.List;

/**
 * Created by Jorge on 23/06/2015.
 */
public class OCRManagementTest extends ActivityInstrumentationTestCase2<StartCameraActivity> {

    private OCRManagement ocrManagement;

    public OCRManagementTest() {
        super(StartCameraActivity.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        ocrManagement = new OCRManagement(getActivity());
    }

    protected void tearDown() throws Exception {
        ocrManagement = null;
        super.tearDown();
    }

    public void test_extract(){

        try {
            List<String> list = ocrManagement.extract();

            assertTrue(!list.isEmpty());

            assertTrue(list.get(0).equals("29694"));
            assertTrue(list.get(1).equals("004"));
            assertTrue(list.get(2).equals("08"));
            assertTrue(list.get(3).equals("0435"));





        } catch (LowConfidenceException e) {
            assertNotNull(null);
        }


    }

}
