package com.example.jorge.fotopremio.gui.asyncTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.facade.Facade;
import com.example.jorge.fotopremio.gui.ShowMessage;
import com.example.jorge.fotopremio.model.IncorrectDataException;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

/**
 * Clase que genera una tarea asincrona cuando se almacena un décimo.
 */
public class AsyncTaskDialogAddDecimo extends AsyncTask<String, String, String> {

    ShowMessage showMessage;
    private Activity activity;
    private ProgressDialog pDialog;
    private List<String> decimoProperties;

    public AsyncTaskDialogAddDecimo(Activity activity, ProgressDialog pDialog, List<String> decimoProperties) {
        this.activity = activity;
        this.pDialog = pDialog;
        this.decimoProperties = decimoProperties;
        showMessage = new ShowMessage();
    }

    @Override
    protected String doInBackground(String... params) {

        Facade facade = Facade.getInstance(activity);
        try {
            facade.addDecimo(decimoProperties.get(0), decimoProperties.get(2),
                    decimoProperties.get(2), decimoProperties.get(3));

            return "OK";

        } catch (IOException e) {
            showMessage.setMessage(activity.getResources().getString(R.string.msgErrorInternet)
                    , activity.getResources().getString(R.string.msgError), null);
            showMessage.show(activity.getFragmentManager(), "errorMsg");
            return null;
        } catch (JSONException e) {
            showMessage.setMessage(activity.getResources().getString(R.string.msgErrorRaffleOut)
                    , activity.getResources().getString(R.string.msgError), null);
            showMessage.show(activity.getFragmentManager(), "errorMsg");
            return null;
        } catch (IncorrectDataException e) {
            showMessage.setMessage(activity.getResources().getString(R.string.msgErrorInsertData)
                    , activity.getResources().getString(R.string.msgError), null);
            showMessage.show(activity.getFragmentManager(), "errorMsg");
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(String... progress) {

    }

    @Override
    protected void onPreExecute() {
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
        pDialog.dismiss();
        if (result != null) {
            Toast.makeText(activity, activity.getResources().getString(R.string.decimoSaved),
                    Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCancelled() {

        pDialog.dismiss();

    }

}
