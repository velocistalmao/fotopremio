package com.example.jorge.fotopremio.manager;

import android.app.Activity;
import android.content.Context;
import android.os.StrictMode;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.base.ConnectionServer;
import com.example.jorge.fotopremio.base.JsonDataRaffle;
import com.example.jorge.fotopremio.model.Decimo;
import com.example.jorge.fotopremio.model.IncorrectDataException;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

/**
 * Clase que administra los décimos.
 */
public class DecimoManagement {

    private Decimo decimo;

    private Activity activity;

    public DecimoManagement(Activity activity) {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.
                Builder().permitNetwork().build());
        decimo = new Decimo();
        this.activity = activity;
    }


    public void setDecimo(String numero, String serie, String fraccion, String nRaffle) throws IOException, JSONException, IncorrectDataException {
        if (validateNumero(numero) && validateSerie(serie) && validateFraccion(fraccion)
                && validateIdRaffle(nRaffle)) {
            decimo.setNumero(format(numero, 5));
            decimo.setSerie(format(serie, 3));
            decimo.setFraccion(format(fraccion, 2));
            extractDataRaffle(nRaffle, activity);
        } else {
            throw new IncorrectDataException("Incorrect data");
        }
    }

    public void setDecimo(String numero, String serie, String fraccion, String euros, String fecha) {
        decimo = new Decimo(numero, euros, serie, fraccion, fecha);
    }

    /**
     * Establece la información del sorteo como fecha, id o el importe.
     *
     * @param nRaffle número de sorteo
     * @throws java.io.IOException
     * @throws org.json.JSONException
     */
    public void extractDataRaffle(String nRaffle, Context context) throws IOException, JSONException {
        String url = context.getResources().getString(R.string.urlJsonIdRaffle);
        ConnectionServer conn = new ConnectionServer();
        JsonDataRaffle json;
        String response;
        List<String> data;

        response = conn.connect(url);
        json = new JsonDataRaffle(response);
        json.jsonDocCharge();
        data = json.getDataRaffle(nRaffle);

        decimo.setFecha(data.get(0));
        decimo.setIdRaffle(data.get(1));
        decimo.setEuros(data.get(2));
    }


    /**
     * Establece a partir de la fecha los euros y el id del sorteo.
     *
     * @return true si ha salido bien sino false
     */
    public boolean setEurosIdRaffle() {

        List<String> list;
        SQLManagement sqlManagement = new SQLManagement(activity);
        list = sqlManagement.getEurosIdRaffle(decimo.getFecha());

        if (!list.isEmpty()) {
            decimo.setEuros(list.get(0));
            decimo.setIdRaffle(list.get(1));
            return true;
        } else {
            return false;
        }

    }

    /**
     * Validador para numero, euros, serie y fracción.
     *
     * @param num  numero
     * @param leng longitud del numero pasado
     * @return el número formateado
     */
    public String format(String num, int leng) {

        if (num.length() < leng && num.length() != 0) {
            do {
                num = "0" + num;
            } while (num.length() != leng);
        } else {
            if (num.length() != leng)
                return "0";
        }
        return num;
    }

    /**
     * Validación del número.
     *
     * @param numero numero del décimo
     * @return true si el formato es válido sino falso
     */
    public boolean validateNumero(String numero) {
        return (numero.length() != 0 && numero.length() <= 5);
    }

    /**
     * Valida el número serie.
     *
     * @param serie número de serie
     * @return true si es válido sino false
     */
    public boolean validateSerie(String serie) {
        return (serie.length() != 0 && serie.length() <= 3);
    }

    /**
     * Valida la fraccion.
     *
     * @param fraccion número de fraccion
     * @return true si el formato es válido sino falso
     */
    public boolean validateFraccion(String fraccion) {
        return (fraccion.length() != 0 && fraccion.length() <= 2);
    }

    /**
     * Valida el nº de sorteo.
     *
     * @param idRaffle nº de sorteo
     * @return true si es válido, sino falso
     */
    public boolean validateIdRaffle(String idRaffle) {
        return idRaffle.length() == 4;
    }


    /**
     * Compara si dos décimos son iguales.
     *
     * @param d décimo a comparar
     * @return true si el formato es válido sino falso
     */
    public boolean compare(Decimo d) {
        return (decimo.getNumero().equals(d.getNumero()) && decimo.getEuros().equals(d.getEuros()) &&
                decimo.getSerie().equals(d.getSerie()) && decimo.getFraccion().equals(d.getFraccion()) &&
                decimo.getFecha().equals(d.getFecha()));
    }

    public Decimo getDecimo() {
        return decimo;
    }

    public void setDecimo(Decimo decimo) {
        this.decimo = decimo;
    }


}
