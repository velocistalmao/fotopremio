package com.example.jorge.fotopremio.base;

import com.example.jorge.fotopremio.model.Decimo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Clase que se encarga de leer ficheros JSON que provienen del servidor de loteria y apuestas
 * del estado.
 */
public class JsonPrizesLotto extends JsonAdmin {

    public JsonPrizesLotto(String doc) {
        super(doc);
    }

    /**
     * Devuelve el premio especial.
     *
     * @return el premio especial
     * @throws org.json.JSONException
     */
    public String returnSpecialPrize() throws JSONException {
        return "" + Integer.parseInt(getFullJson().getJSONObject("premioEspecial").getString("premio")) / 100;
    }

    /**
     * Comprueba si el numero pasado es el premio especial.
     *
     * @param decimo decimo a comprobar
     * @return si es premio especial o no
     * @throws JSONException
     */
    public boolean isSpecialPrize(Decimo decimo) throws JSONException {
        JSONObject premioEspecial = getFullJson().getJSONObject("premioEspecial");
        String serie, fraccion;

        serie = premioEspecial.getString("serie");
        if (serie.length() == 1)
            serie = "00" + serie;
        else if (serie.length() == 2)
            serie = "0" + serie;

        fraccion = premioEspecial.getString("fraccion");
        if (fraccion.length() == 1)
            fraccion = "0" + fraccion;

        return (premioEspecial.getString("numero").equals("0" + decimo.getNumero()) &&
                serie.equals(decimo.getSerie()) &&
                fraccion.equals(decimo.getFraccion()));
    }


    /**
     * Comprueba si el numero tiene premio.
     *
     * @param numero número a comprobar
     * @return el premio
     * @throws JSONException
     */
    public String numberCheck(String numero) throws JSONException {

        JSONArray compruebe = getFullJson().getJSONArray("compruebe");
        JSONObject nextObject;

        for (int i = 0; i < compruebe.length(); i++) {
            nextObject = compruebe.getJSONObject(i);
            if (nextObject.getString("decimo").equals("0" + numero)) {
                return "" + (Integer.parseInt(nextObject.getString("prize")) / 100);
            }
        }
        return "0";
    }

}
