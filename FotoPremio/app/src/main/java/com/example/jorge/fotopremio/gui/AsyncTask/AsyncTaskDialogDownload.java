package com.example.jorge.fotopremio.gui.asyncTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Clase que genera una tarea asincrona donde se descarga un fichero.
 */
public class AsyncTaskDialogDownload extends AsyncTask<String, Integer, String> {

    /**
     * Tipo de progreso.
     */
    private ProgressDialog pDialog;

    /**
     * Url del fichero.
     */
    private String url;

    /**
     * Tamaño del fichero.
     */
    private int lenghtOfFile;

    /**
     * Actividad donde se ejecuta esta clase.
     */
    private Activity activity;

    public AsyncTaskDialogDownload(ProgressDialog pDialog, String url, int lenghtOfFile, Activity activity) {
        this.pDialog = pDialog;
        this.url = url;
        this.lenghtOfFile = lenghtOfFile;
        this.activity = activity;
    }

    protected String doInBackground(String... params) {

        try {
            URL u = new URL(url);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            File SDCardRoot = new File(Environment.getExternalStorageDirectory() + "/Boletos/tessdata");
            SDCardRoot.mkdirs();
            File file = new File(SDCardRoot, "spa.traineddata");

            FileOutputStream f = new FileOutputStream(file);
            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1;
            long total = 0;
            while ((len1 = in.read(buffer)) > 0) {
                total += len1;
                publishProgress((int) ((total * 100) / lenghtOfFile));
                f.write(buffer, 0, len1);
            }
            f.close();

        } catch (Exception e) {
            Toast.makeText(activity, "Error con la conexion", Toast.LENGTH_LONG).show();
            cancel(true);
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        pDialog.setProgress(progress[0].intValue());
    }

    @Override
    protected void onPreExecute() {

        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                AsyncTaskDialogDownload.this.cancel(true);
            }
        });

        pDialog.setProgress(0);
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
        pDialog.dismiss();
    }

    @Override
    protected void onCancelled() {

    }

}
