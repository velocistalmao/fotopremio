package com.example.jorge.fotopremio.gui.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.facade.Facade;
import com.example.jorge.fotopremio.gui.ShowMessage;
import com.example.jorge.fotopremio.gui.asyncTask.AsyncTaskDialogCheck;

/**
 * Detalles de un décimo guardado.
 */
public class DecimoDetailActivity extends Activity {

    ShowMessage error;
    private TextView detail_numero,
            detail_euros,
            detail_serie,
            detail_fraccion,
            detail_fecha;
    private Facade facade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        error = new ShowMessage();

        Bundle bundle = getIntent().getExtras();

        facade = Facade.getInstance(this);
        facade.setDecimo(bundle.getString("numero"), bundle.getString("serie"),
                bundle.getString("fraccion"), bundle.getString("euros"), bundle.getString("fecha"));

        detail_numero = (TextView) findViewById(R.id.d_numero);
        detail_euros = (TextView) findViewById(R.id.d_euros);
        detail_serie = (TextView) findViewById(R.id.d_serie);
        detail_fraccion = (TextView) findViewById(R.id.d_fraccion);
        detail_fecha = (TextView) findViewById(R.id.d_fecha);

        detail_numero.setText(facade.getDecimo().getNumero());
        detail_euros.setText(facade.getDecimo().getEuros());
        detail_serie.setText(facade.getDecimo().getSerie());
        detail_fraccion.setText(facade.getDecimo().getFraccion());
        detail_fecha.setText(facade.getDecimo().getFecha());
    }

    public void onClickBtn_checkDetail(View view) {

        if (facade.setEurosIdRaffle()) {

            ProgressDialog pDialog;
            pDialog = new ProgressDialog(DecimoDetailActivity.this);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.setMessage(getResources().getString(R.string.checking));
            pDialog.setCancelable(false);
            pDialog.setMax(100);

            AsyncTaskDialogCheck asyncTaskDialogCheck = new AsyncTaskDialogCheck(this, pDialog, facade.getDecimo(), null);
            asyncTaskDialogCheck.execute();

        } else {
            error.setMessage(getResources().getString(R.string.msgErrorEmptyList)
                    , getResources().getString(R.string.msgError), null);
            error.show(getFragmentManager(), "errorMsg");
        }


    }
}
