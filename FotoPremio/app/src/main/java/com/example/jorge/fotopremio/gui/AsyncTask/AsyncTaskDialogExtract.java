package com.example.jorge.fotopremio.gui.asyncTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.facade.Facade;
import com.example.jorge.fotopremio.gui.ShowMessage;
import com.example.jorge.fotopremio.gui.view.VisualExtractActivity;
import com.example.jorge.fotopremio.model.LowConfidenceException;

import java.util.List;

/**
 * Clase que genera una tarea asincrona donde se extraen los datos de una imagen.
 */
public class AsyncTaskDialogExtract extends AsyncTask<String, Integer, String> {

    ShowMessage showMessage;
    List<String> data;
    private Activity activity;
    private ProgressDialog pDialog;

    public AsyncTaskDialogExtract(Activity activity, ProgressDialog pDialog) {
        this.activity = activity;
        this.pDialog = pDialog;
        data = null;
    }

    @Override
    protected String doInBackground(String... params) {

        Facade facade = Facade.getInstance(activity);
        showMessage = new ShowMessage();


        try {
            data = facade.extractData();
            facade.startActivity(data, VisualExtractActivity.class);
        } catch (LowConfidenceException e) {
            showMessage.setMessage(activity.getResources().getString(R.string.msgErrorConfidence)
                    , activity.getResources().getString(R.string.msgError), null);
            showMessage.show(activity.getFragmentManager(), "errorMsg");
        }

        cancel(true);
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {

    }

    @Override
    protected void onPreExecute() {
        pDialog.setProgress(0);
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
        pDialog.dismiss();
      /*  Intent i = new Intent(activity, VisualExtractActivity.class);
        i.putExtra("numero", data.get(0));
        i.putExtra("serie",  data.get(1));
        i.putExtra("fraccion", data.get(2));
        i.putExtra("idRaffle", data.get(3));
        activity.getApplicationContext().startActivity(i);*/
        //activity.finish();
    }

    @Override
    protected void onCancelled() {

        pDialog.dismiss();

    }

    public List<String> getData() {
        return data;
    }
}
