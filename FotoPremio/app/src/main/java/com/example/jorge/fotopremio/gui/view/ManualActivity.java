package com.example.jorge.fotopremio.gui.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.EditText;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.facade.Facade;
import com.example.jorge.fotopremio.gui.ShowMessage;
import com.example.jorge.fotopremio.gui.asyncTask.AsyncTaskDialogAddDecimo;
import com.example.jorge.fotopremio.gui.asyncTask.AsyncTaskDialogCheck;
import com.example.jorge.fotopremio.model.IncorrectDataException;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Clase que maneja el activity_manual.
 */
public class ManualActivity extends Activity {

    ShowMessage showMessage;
    private EditText ed_numero,
            ed_serie,
            ed_fraccion,
            ed_raffle;
    private Facade facade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual);

        ed_numero = (EditText) findViewById(R.id.ed_manual_numero);
        ed_fraccion = (EditText) findViewById(R.id.ed_manual_fraccion);
        ed_serie = (EditText) findViewById(R.id.ed_manual_serie);
        ed_raffle = (EditText) findViewById(R.id.ed_manual_raffle);

        facade = Facade.getInstance(this);

        showMessage = new ShowMessage();

        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.
                Builder().permitNetwork().build());
    }


    /**
     * Guarda los datos del décimo en la base de datos.
     *
     * @param view vista
     */
    public void onClickBtn_save(View view) {

        List<String> decimoProperties = new ArrayList<>();

        decimoProperties.add(ed_numero.getText().toString());
        decimoProperties.add(ed_serie.getText().toString());
        decimoProperties.add(ed_fraccion.getText().toString());
        decimoProperties.add(ed_raffle.getText().toString());

        ProgressDialog pDialog;
        pDialog = new ProgressDialog(ManualActivity.this);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setMessage(getResources().getString(R.string.Saving));
        pDialog.setCancelable(false);
        pDialog.setMax(100);

        AsyncTaskDialogAddDecimo asyncTaskDialogAddDecimo = new AsyncTaskDialogAddDecimo(this, pDialog, decimoProperties);
        asyncTaskDialogAddDecimo.execute();

        ed_fraccion.setText("");
        ed_serie.setText("");
        ed_numero.setText("");
        ed_raffle.setText("");


    }


    public void onClickBtn_check(View view) {

        try {
            facade.setDecimo(ed_numero.getText().toString(), ed_serie.getText().toString(),
                    ed_fraccion.getText().toString(), ed_raffle.getText().toString());


            ProgressDialog pDialog;
            pDialog = new ProgressDialog(ManualActivity.this);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.setMessage(getResources().getString(R.string.checking));
            pDialog.setCancelable(false);
            pDialog.setMax(100);

            AsyncTaskDialogCheck asyncTaskDialogCheck = new AsyncTaskDialogCheck(this, pDialog, facade.getDecimo(), ed_raffle.getText().toString());
            asyncTaskDialogCheck.execute();

        } catch (IOException e) {
            showMessage.setMessage(getResources().getString(R.string.msgErrorInternet)
                    , getResources().getString(R.string.msgError), null);
            showMessage.show(getFragmentManager(), "errorMsg");
        } catch (JSONException e) {
            showMessage.setMessage(getResources().getString(R.string.msgErrorRaffleOut)
                    , getResources().getString(R.string.msgError), null);
            showMessage.show(getFragmentManager(), "errorMsg");
        } catch (IncorrectDataException e) {
            showMessage.setMessage(getResources().getString(R.string.msgErrorInsertData)
                    , getResources().getString(R.string.msgError), null);
            showMessage.show(getFragmentManager(), "errorMsg");
        }
    }

    public void onClick_imgNumHelp(View view) {
        showMessage.setMessage(getResources().getString(R.string.msg_help_numero)
                , getResources().getString(R.string.msg_title_help_numero), null);
        showMessage.show(getFragmentManager(), "helpMsg");
    }

    public void onClick_imgSerieHelp(View view) {
        showMessage.setMessage(getResources().getString(R.string.msg_help_serie)
                , getResources().getString(R.string.msg_title_help_serie), null);
        showMessage.show(getFragmentManager(), "helpMsg");
    }

    public void onClick_imgFracHelp(View view) {
        showMessage.setMessage(getResources().getString(R.string.msg_help_fraccion)
                , getResources().getString(R.string.msg_title_help_fraccion), null);
        showMessage.show(getFragmentManager(), "helpMsg");
    }

    public void onClick_imgRaffleHelp(View view) {
        showMessage.setMessage(getResources().getString(R.string.msg_help_raffle)
                , getResources().getString(R.string.msg_title_help_raffle), null);
        showMessage.show(getFragmentManager(), "helpMsg");
    }

}
