package com.example.jorge.fotopremio.base;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que se encarga de leer ficheros JSON que provienen un host privado en el que tenemos una
 * serie de características de un determinado sorteo.
 */
public class JsonDataRaffle extends JsonAdmin {

    /**
     * Contructuor.
     *
     * @param doc documento JSON
     */
    public JsonDataRaffle(String doc) {
        super(doc);
    }

    /**
     * Extrae los datos del sorteo.
     *
     * @param nRaffle número del sorteo
     * @return lista con la fecha, id de la url y euros del sorteo
     * @throws org.json.JSONException
     */
    public List<String> getDataRaffle(String nRaffle) throws JSONException {

        List<String> dataRaffle = new ArrayList<>();
        JSONObject raffle = getFullJson().getJSONObject(nRaffle);

        dataRaffle.add(raffle.getString("fecha"));
        dataRaffle.add(raffle.getString("id"));
        dataRaffle.add(raffle.getString("euros"));

        return dataRaffle;

    }
}
