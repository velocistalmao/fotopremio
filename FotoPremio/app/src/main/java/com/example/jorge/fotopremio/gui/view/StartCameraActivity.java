package com.example.jorge.fotopremio.gui.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.gui.ShowMessage;
import com.example.jorge.fotopremio.gui.asyncTask.AsyncTaskDialogDownload;
import com.example.jorge.fotopremio.gui.asyncTask.AsyncTaskDialogExtract;

import java.io.File;


/**
 * Clase que maneja la actividad activity_camera.
 */
public class StartCameraActivity extends Activity {

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    ShowMessage help;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        ProgressDialog pDialog;
        help = new ShowMessage();
        img = (ImageView) findViewById(R.id.img_foto);
        Bitmap bMap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() +
                "/Boletos/" + getResources().getString(R.string.imgName));

        img.setImageBitmap(bMap);

        File file = new File(Environment.getExternalStorageDirectory() + "/Boletos/tessdata");
        if (!file.exists()) {
            String url = getResources().getString(R.string.urlTessdata);

            pDialog = new ProgressDialog(StartCameraActivity.this);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setMessage(getResources().getString(R.string.msgDownloadOCR));
            pDialog.setCancelable(false);
            pDialog.setMax(100);

            AsyncTaskDialogDownload asyncTask = new AsyncTaskDialogDownload(pDialog, url, getResources().getInteger(R.integer.tessdataSize), this);
            asyncTask.execute(url);
        }
    }

    public void onClickBtn_start_camera(View view) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "Boletos");
        imagesFolder.mkdirs();
        File image = new File(imagesFolder, getResources().getString(R.string.imgName));
        Uri uriSavedImage = Uri.fromFile(image);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
        //Lanzamos la apliacion de la camara con retorno
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    public void onClickBtn_extract(View view) {

        if (new File(Environment.getExternalStorageDirectory() + "/Boletos/billete.jpg").exists()) {
            ProgressDialog pDialog;
            pDialog = new ProgressDialog(StartCameraActivity.this);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.setMessage(getResources().getString(R.string.msgOCR));
            pDialog.setCancelable(false);
            pDialog.setMax(100);


            AsyncTaskDialogExtract extractTask = new AsyncTaskDialogExtract(StartCameraActivity.this,
                    pDialog);
            extractTask.execute();

        } else {
            ShowMessage error = new ShowMessage();
            error.setMessage(getResources().getString(R.string.msgImageDontCharged)
                    , getResources().getString(R.string.msgError), null);
            error.show(getFragmentManager(), "errorMsg");
        }


    }

    /**
     * Resultado de la captura de la foto.
     *
     * @param requestCode código de respuesta
     * @param resultCode  código del resultado
     * @param intent      intent
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bitmap bMap = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() +
                        "/Boletos/" + getResources().getString(R.string.imgName));
                img.setImageBitmap(bMap);
                Toast.makeText(this, getResources().getString(R.string.msgFotoFinish), Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, getResources().getString(R.string.msgCancelCamera), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, getResources().getString(R.string.msgErrorFoto), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onClick_imgHelp(View view) {
        help.setMessage(getResources().getString(R.string.help_camera)
                , getResources().getString(R.string.help_title_camera), null);
        help.show(getFragmentManager(), "helpMsg");
    }

}


