package com.example.jorge.fotopremio.manager;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.model.LowConfidenceException;
import com.googlecode.tesseract.android.TessBaseAPI;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que administra el manejo del OCR.
 */
public class OCRManagement {

    private static final int HEIGHT = 32;
    private static final int WIDTH = 76;

    private String dataExtract;
    private Activity activity;

    public OCRManagement(Activity activity) {
        dataExtract = null;
        this.activity = activity;
    }

    public List<String> extract() throws LowConfidenceException {

        String DATA_PATH = Environment.getExternalStorageDirectory() + "/Boletos/";
        int confidence;
        List<String> data = new ArrayList<>();

        TessBaseAPI baseApi = new TessBaseAPI();
        baseApi.init(DATA_PATH, "spa");
        Bitmap myImage = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() +
                "/Boletos/" + "billete.jpg");
        Bitmap imgaux;
        imgaux = Bitmap.createBitmap(myImage, (myImage.getWidth() * HEIGHT / 100), myImage.getHeight() * WIDTH / 100,
                myImage.getWidth() - (myImage.getWidth() * HEIGHT / 100), myImage.getHeight() - (myImage.getHeight() * WIDTH / 100));
        baseApi.setImage(imgaux);
        dataExtract = baseApi.getUTF8Text();
        dataExtract = dataExtract.replaceAll("\\D", "");
        confidence = baseApi.meanConfidence();
        baseApi.end();

        if (confidence > activity.getResources().getInteger(R.integer.confidenceMin)) {
            if (dataExtract.length() == 20) {

                data.add(dataExtract.substring(11, 16));
                data.add(dataExtract.substring(7, 10));
                data.add(dataExtract.substring(5, 7));
                data.add(dataExtract.substring(1, 5));

            } else {
                throw new LowConfidenceException("Low confidence");
            }
        } else {
            throw new LowConfidenceException("Low confidence");
        }
        return data;

    }
}
