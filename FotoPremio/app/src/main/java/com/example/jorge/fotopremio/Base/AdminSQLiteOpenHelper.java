package com.example.jorge.fotopremio.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Clase que se encarga de crear y actualizar la base de datos.
 */
public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {

    /**
     * Contructor de la clase.
     *
     * @param context contexto
     * @param name    nombre
     * @param factory factory
     * @param version version
     */
    public AdminSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * Método que se encarga de crear la base de datos
     *
     * @param db BBDD
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table decimos(_id INTEGER PRIMARY KEY AUTOINCREMENT, numero text not null," +
                " euros text not null, serie text not null, fraccion text not null, " +
                "fecha text not null, sort text not null, idRaffle text not null)");
    }

    /**
     * Método que se encarga de actualizar la base de datos.
     *
     * @param db         BBDD
     * @param oldVersion version anterior
     * @param newVersion nueva version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //not null al id, probar
        db.execSQL("drop table if exists decimos");
        db.execSQL("create table decimos(_id INTEGER PRIMARY KEY AUTOINCREMENT, numero text not null," +
                "euros text not null, serie text not null, fraccion text not null, " +
                "fecha text not null, sort text not null, idRaffle text not null)");

    }


}
