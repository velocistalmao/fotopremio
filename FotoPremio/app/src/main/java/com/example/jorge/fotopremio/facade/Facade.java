package com.example.jorge.fotopremio.facade;

import android.app.Activity;
import android.content.Intent;

import com.example.jorge.fotopremio.manager.CheckPrizeManagement;
import com.example.jorge.fotopremio.manager.DecimoManagement;
import com.example.jorge.fotopremio.manager.OCRManagement;
import com.example.jorge.fotopremio.manager.SQLManagement;
import com.example.jorge.fotopremio.model.Decimo;
import com.example.jorge.fotopremio.model.IncorrectDataException;
import com.example.jorge.fotopremio.model.LowConfidenceException;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

/**
 * Clase fachada.
 */
public class Facade {

    private static Facade INSTANCE;
    private Activity activity;
    private Decimo decimo;

    private Facade(Activity activity) {
        this.activity = activity;
    }

    public static Facade getInstance(Activity activity) {
        if (INSTANCE == null)
            INSTANCE = new Facade(activity);

        return INSTANCE;
    }

    /**
     * Extrae los datos del décimo.
     *
     * @return una lista con los datos
     * @throws LowConfidenceException
     */
    public List<String> extractData() throws LowConfidenceException {

        OCRManagement ocrManagement = new OCRManagement(activity);
        return ocrManagement.extract();

    }

    /**
     * Lanza una nueva actividad.
     *
     * @param data     datos de la actividad
     * @param newClass clase  que manejará la actividad
     */
    public void startActivity(List<String> data, Class newClass) {
        Intent i = new Intent(activity, newClass);
        i.putExtra("numero", data.get(0));
        i.putExtra("serie", data.get(1));
        i.putExtra("fraccion", data.get(2));
        i.putExtra("idRaffle", data.get(3));
        activity.startActivity(i);
    }

    /**
     * Añade a la BBDD un nuevo décimo
     *
     * @param numero   numero
     * @param serie    serie
     * @param fraccion fraccion
     * @param raffle   numero de sorteo
     * @throws IOException
     * @throws JSONException
     * @throws IncorrectDataException
     */
    public void addDecimo(String numero, String serie, String fraccion, String raffle) throws
            IOException, JSONException, IncorrectDataException {

        int sort;
        Decimo decimo;

        DecimoManagement decimoManagement = new DecimoManagement(activity);
        decimoManagement.setDecimo(numero, serie, fraccion, raffle);
        decimo = decimoManagement.getDecimo();

        sort = Integer.parseInt(decimo.getFecha().substring(6, 10) +
                decimo.getFecha().substring(3, 5) + decimo.getFecha().substring(0, 2));

        SQLManagement sqlManagement = new SQLManagement(activity);
        sqlManagement.addDecimo(decimo, sort);

    }

    public void addDecimo(Decimo decimo) {

        int sort;
        sort = Integer.parseInt(decimo.getFecha().substring(6, 10) +
                decimo.getFecha().substring(3, 5) + decimo.getFecha().substring(0, 2));

        SQLManagement sqlManagement = new SQLManagement(activity);
        sqlManagement.addDecimo(decimo, sort);

    }


    public void deleteDecimo(Decimo decimo) {
        SQLManagement sqlManagement = new SQLManagement(activity);

        sqlManagement.deleteDecimo(sqlManagement.getIdDecimo(decimo));
    }

    public void deleteAll() {
        SQLManagement sqlManagement = new SQLManagement(activity);
        sqlManagement.deleteAll();
    }


    public void setDecimo(String numero, String serie, String fraccion, String raffle)
            throws IOException, JSONException, IncorrectDataException {
        DecimoManagement decimoManagement = new DecimoManagement(activity);
        decimoManagement.setDecimo(numero, serie, fraccion, raffle);
        decimo = decimoManagement.getDecimo();
    }

    public void setDecimo(String numero, String serie, String fraccion, String euros, String fecha) {
        DecimoManagement decimoManagement = new DecimoManagement(activity);
        decimoManagement.setDecimo(numero, serie, fraccion, euros, fecha);
        decimo = decimoManagement.getDecimo();
    }

    public Decimo getDecimo() {
        return decimo;
    }

    public boolean setEurosIdRaffle() {
        DecimoManagement decimoManagement = new DecimoManagement(activity);
        decimoManagement.setDecimo(decimo);
        if (decimoManagement.setEurosIdRaffle()) {
            decimo = decimoManagement.getDecimo();
            return true;
        }

        return false;
    }

    public String getPrize(Decimo decimo, String idRaffle) throws IOException, JSONException {
        CheckPrizeManagement checkPrizeManagement = new CheckPrizeManagement(activity);
        return checkPrizeManagement.getPrize(decimo, idRaffle);
    }

    public List<Decimo> getDecimoList() {
        SQLManagement sqlManagement = new SQLManagement(activity);
        return sqlManagement.getDecimosList();
    }


}
