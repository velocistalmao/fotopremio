package com.example.jorge.fotopremio.gui.asyncTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.facade.Facade;
import com.example.jorge.fotopremio.gui.ShowMessage;
import com.example.jorge.fotopremio.model.Decimo;

import org.json.JSONException;

import java.io.IOException;

/**
 * Clase que genera una tarea asincrona cuando se hace una consulta del premio del décimo.
 */
public class AsyncTaskDialogCheck extends AsyncTask<String, String, String> {

    ShowMessage showMessage;
    private Activity activity;
    private ProgressDialog pDialog;
    private Decimo decimo;
    private String idRaffle;

    public AsyncTaskDialogCheck(Activity activity, ProgressDialog pDialog, Decimo decimo, String idRaffle) {
        this.activity = activity;
        this.pDialog = pDialog;
        this.decimo = decimo;
        this.idRaffle = idRaffle;
        showMessage = new ShowMessage();
    }

    @Override
    protected String doInBackground(String... params) {

        Facade facade;
        facade = Facade.getInstance(activity);
        try {
            return facade.getPrize(decimo, idRaffle);
        } catch (IOException e) {
            showMessage.setMessage(activity.getResources().getString(R.string.msgErrorInternet)
                    , activity.getResources().getString(R.string.msgError), null);
            showMessage.show(activity.getFragmentManager(), "errorMsg");
            return null;
        } catch (JSONException e) {
            showMessage.setMessage(activity.getResources().getString(R.string.msgErrorRaffleOut)
                    , activity.getResources().getString(R.string.msgError), null);
            showMessage.show(activity.getFragmentManager(), "errorMsg");
            return null;
        }

    }

    @Override
    protected void onProgressUpdate(String... progress) {

    }

    @Override
    protected void onPreExecute() {
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
        pDialog.dismiss();
        if (result != null) {
            showMessage.setMessage(activity.getResources().getString(R.string.msg_prize)
                    , activity.getResources().getString(R.string.msg_title_prize), result);
            showMessage.show(activity.getFragmentManager(), "prizeMsg");
        }

    }

    @Override
    protected void onCancelled() {

        pDialog.dismiss();

    }

}
