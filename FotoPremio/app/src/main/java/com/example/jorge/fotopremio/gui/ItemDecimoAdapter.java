package com.example.jorge.fotopremio.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.model.Decimo;

import java.util.List;

/**
 * Clase que genera un Adaptador.
 */
public class ItemDecimoAdapter extends BaseAdapter {

    protected Context context;
    protected List<Decimo> decimos;

    /**
     * Constructor que establece el contezto y el array de décimos.
     *
     * @param context contexto desde el que se llama
     * @param decimos array de décimos
     */
    public ItemDecimoAdapter(Context context, List<Decimo> decimos) {
        this.context = context;
        this.decimos = decimos;
    }

    /**
     * Devuelve el número de décimos.
     *
     * @return número de décimos
     */
    @Override
    public int getCount() {
        return decimos.size();
    }

    /**
     * Devuelve un décimo que esté en una determina posición.
     *
     * @param position posición
     * @return décimo
     */
    @Override
    public Object getItem(int position) {
        return decimos.get(position);
    }

    /**
     * Devuelve el id del décimo.
     *
     * @param position posición
     * @return id
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Devuelve la vista del adaptador.
     *
     * @param position    posición
     * @param convertView vista reutilizada
     * @param parent      parent
     * @return vista
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.item_list, parent, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.lottoIcon);
        TextView numberList = (TextView) itemView.findViewById(R.id.numberList);
        TextView description = (TextView) itemView.findViewById(R.id.description);

        imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.lotto_icon));
        numberList.setText(decimos.get(position).getNumero());
        description.setText(decimos.get(position).viewSubItem());

        return itemView;

    }
}
