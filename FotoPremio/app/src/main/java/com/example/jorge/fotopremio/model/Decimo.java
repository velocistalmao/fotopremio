package com.example.jorge.fotopremio.model;

/**
 * Clase que se encarga del tratamiento de un décimo.
 */
public class Decimo {

    /**
     * Número del décimo.
     */
    private String numero;

    /**
     * Cantidad invertida en el decimo en euros.
     */
    private String euros;

    /**
     * Número de la serie del décimo.
     */
    private String serie;

    /**
     * Número de la fracción del décimo.
     */
    private String fraccion;

    /**
     * Fecha del sorteo del décimo.
     */
    private String fecha;

    /**
     * Id del sorteo.
     */
    private String idRaffle;

    /**
     * Contructor por defecto.
     */
    public Decimo() {
    }

    /**
     * Contructor al que le pasan las propiedades de un décimo.
     *
     * @param numero   numero
     * @param euros    importe en euros
     * @param serie    serie
     * @param fraccion fracción
     * @param fecha    fecha del sorteo
     */
    public Decimo(String numero, String euros, String serie, String fraccion, String fecha) {
        this.numero = numero;
        this.euros = euros;
        this.serie = serie;
        this.fraccion = fraccion;
        this.fecha = fecha;

    }

    /**
     * Devuelve una cadena para visualizarla en un listview.
     *
     * @return string
     */
    public String viewSubItem() {
        return fecha + " " + serie + " " + fraccion + " " + euros + "€";
    }

    /**
     * Devuelve la fecha del sorteo.
     *
     * @return fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Establece la fecha del sorteo.
     *
     * @param fecha fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * Devuelve el id del sorteo.
     *
     * @return d del sorteo
     */
    public String getIdRaffle() {
        return idRaffle;
    }

    /**
     * Estableve el id del sorteo.
     *
     * @param idRaffle d del sorteo
     */
    public void setIdRaffle(String idRaffle) {
        this.idRaffle = idRaffle;
    }

    /**
     * Devuelve el número del décimo.
     *
     * @return numero del décimo
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Establece el número del décimo.
     *
     * @param numero número del décimo
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * Devuelve los euros invertidos en el décimo.
     *
     * @return euros invertidos
     */
    public String getEuros() {
        return euros;
    }

    /**
     * Establece los euros invertidos en el décimo.
     *
     * @param euros euros invertidos
     */
    public void setEuros(String euros) {
        this.euros = euros;
    }

    /**
     * Devuelve el número de serie.
     *
     * @return número de serie
     */
    public String getSerie() {
        return serie;
    }

    /**
     * Establece el número de serie.
     *
     * @param serie número de serie
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }

    /**
     * Devuelve el número de fracción.
     *
     * @return número de fracción
     */
    public String getFraccion() {
        return fraccion;
    }

    /**
     * Establece el número de fracción.
     *
     * @param fraccion número de fracción
     */
    public void setFraccion(String fraccion) {
        this.fraccion = fraccion;
    }


}
