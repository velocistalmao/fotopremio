package com.example.jorge.fotopremio.manager;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.jorge.fotopremio.base.AdminSQLiteOpenHelper;
import com.example.jorge.fotopremio.model.Decimo;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que se encarga de manejar operaciones en la base de datos.
 */
public class SQLManagement {

    /**
     * Base de datos.
     */
    SQLiteDatabase bd;
    /**
     * Administración de la bbdd.
     */
    private AdminSQLiteOpenHelper admin;
    private Activity activity;

    /**
     * Contructor.
     *
     * @param activity actividad desde el que se llamará esta clase
     */
    public SQLManagement(Activity activity) {
        this.activity = activity;
        admin = new AdminSQLiteOpenHelper(activity, "admin", null, 1);
    }

    /**
     * Método que borra un decimo en la bbdd.
     *
     * @param id id del décimo
     */
    public void deleteDecimo(int id) {
        bd = admin.getWritableDatabase();
        bd.execSQL("DELETE FROM decimos WHERE _id=" + id + "");
        bd.close();
    }

    /**
     * Método que devuelve el id del decimo pasado.
     *
     * @param decimo decimo
     * @return id del décimo pasado
     * @throws IOException
     * @throws JSONException
     */
    public int getIdDecimo(Decimo decimo) {

        DecimoManagement decimoManagement = new DecimoManagement(activity);
        decimoManagement.setDecimo(decimo);

        bd = admin.getWritableDatabase();
        Cursor cursor = bd.rawQuery("select _id, numero, euros, serie, fraccion, fecha from decimos order by sort", null);
        if (cursor.moveToFirst()) {
            do {
                if (decimoManagement.compare(new Decimo(cursor.getString(1), cursor.getString(2), cursor.getString(3),
                        cursor.getString(4), cursor.getString(5)))) {
                    bd.close();
                    return Integer.parseInt(cursor.getString(0));
                }
            } while (cursor.moveToNext());
        }
        bd.close();

        return 0;

    }

    /**
     * Método que borra todos los decimos de la tabla.
     */
    public void deleteAll() {
        bd = admin.getWritableDatabase();
        bd.execSQL("delete from decimos");
        bd.close();
    }

    /**
     * Método que devuelve una lista de décimos de la bbdd.
     *
     * @return lista de décimos
     */
    public List<Decimo> getDecimosList() {

        bd = admin.getWritableDatabase();
        Cursor cursor = bd.rawQuery("select numero, euros, serie, fraccion, fecha from decimos order by sort", null);
        Decimo decimo;

        List<Decimo> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                decimo = new Decimo(cursor.getString(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4));
                list.add(decimo);
            } while (cursor.moveToNext());
        }

        bd.close();

        return list;

    }

    /**
     * Añade un décimo a la bbdd.
     *
     * @param decimo decimo a almacenar
     * @param sort   campo de bbdd que maneja el orden de fechas
     */
    public void addDecimo(Decimo decimo, int sort) {

        bd = admin.getWritableDatabase();
        ContentValues registry = new ContentValues();
        registry.put("numero", decimo.getNumero());
        registry.put("euros", decimo.getEuros());
        registry.put("serie", decimo.getSerie());
        registry.put("fraccion", decimo.getFraccion());
        registry.put("fecha", decimo.getFecha());
        registry.put("sort", sort);
        registry.put("idRaffle", decimo.getIdRaffle());

        bd.insert("decimos", null, registry);
        bd.close();

    }

    /**
     * Método que devuelve los euros y el id del sorteo en una lista.
     *
     * @param fecha fecha del décimo
     * @return una lista con la primera posición los euros y en la segunda el id del sorteo
     */
    public List<String> getEurosIdRaffle(String fecha) {

        List<String> list = new ArrayList<>();
        bd = admin.getWritableDatabase();

        Cursor cursor = bd.rawQuery("select fecha, euros, idRaffle from decimos order by sort", null);
        if (cursor.moveToFirst()) {
            do {
                if (fecha.equals(cursor.getString(0))) {
                    list.add(cursor.getString(1));
                    list.add(cursor.getString(2));
                    bd.close();
                    return list;
                }
            } while (cursor.moveToNext());
        }

        bd.close();
        return list;

    }

}
