package com.example.jorge.fotopremio.manager;

import android.app.Activity;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.base.ConnectionServer;
import com.example.jorge.fotopremio.base.JsonPrizesLotto;
import com.example.jorge.fotopremio.model.Decimo;

import org.json.JSONException;

import java.io.IOException;

/**
 * Clase que administra las consultas de premios.
 */
public class CheckPrizeManagement {

    private Activity activity;

    public CheckPrizeManagement(Activity activity) {
        this.activity = activity;
    }


    public String getPrize(Decimo decimo, String idRaffle) throws IOException, JSONException {

        ConnectionServer conn = new ConnectionServer();

        JsonPrizesLotto json;
        String response;
        String prize;

        DecimoManagement decimoManagement = new DecimoManagement(activity);
        decimoManagement.setDecimo(decimo);


        if (idRaffle != null)
            decimoManagement.extractDataRaffle(idRaffle, activity);

        if (decimo.getIdRaffle() != null) {
            String url = activity.getResources().getString(R.string.urlPrizeRequest) +
                    decimo.getIdRaffle();

            response = conn.connect(url);
            json = new JsonPrizesLotto(response);
            json.jsonDocCharge();
            if (json.isSpecialPrize(decimo))
                prize = json.returnSpecialPrize();
            else
                prize = json.numberCheck(decimo.getNumero());
            return prize;

        }

        return null;
    }


}
