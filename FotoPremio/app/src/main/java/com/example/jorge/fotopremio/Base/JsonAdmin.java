package com.example.jorge.fotopremio.base;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Esta clase permite trabajar con ficheros JSON.
 */
public abstract class JsonAdmin {


    /**
     * Documento que te pasan a través de una URL.
     */
    private String doc;

    /**
     * Fichero JSON entero.
     */
    private JSONObject fullJson;

    /**
     * Contructor.
     *
     * @param doc documento a leer
     */
    public JsonAdmin(String doc) {
        this.doc = doc;
    }

    /**
     * Pasa el fichero json a un JSONObject y lee el importe por defecto.
     *
     * @throws JSONException
     */
    public void jsonDocCharge() throws JSONException {
        fullJson = new JSONObject(doc);
    }


    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public JSONObject getFullJson() {
        return fullJson;
    }

    public void setFullJson(JSONObject fullJson) {
        this.fullJson = fullJson;
    }

}
