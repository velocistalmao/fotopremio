package com.example.jorge.fotopremio.model;

/**
 * Clase que genera una excepción por baja confianza.
 */
public class LowConfidenceException extends Exception {

    public LowConfidenceException(String message) {
        super(message);
    }

}
