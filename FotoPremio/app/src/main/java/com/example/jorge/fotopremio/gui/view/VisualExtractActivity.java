package com.example.jorge.fotopremio.gui.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.facade.Facade;
import com.example.jorge.fotopremio.gui.ShowMessage;
import com.example.jorge.fotopremio.gui.asyncTask.AsyncTaskDialogCheck;
import com.example.jorge.fotopremio.model.IncorrectDataException;

import org.json.JSONException;

import java.io.IOException;

/**
 * Clase que visualiza en un layout lo extraído en una imagen.
 */
public class VisualExtractActivity extends Activity {

    ShowMessage error;
    private TextView tv_numero,
            tv_serie,
            tv_fraccion,
            tv_euros,
            tv_fecha;
    private Facade facade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extract);

        tv_numero = (TextView) findViewById(R.id.tv_manual_numero);
        tv_euros = (TextView) findViewById(R.id.tv_manual_euros);
        tv_fraccion = (TextView) findViewById(R.id.tv_manual_fraccion);
        tv_serie = (TextView) findViewById(R.id.tv_manual_serie);
        tv_fecha = (TextView) findViewById(R.id.tv_manual_fecha);

        Bundle bundle = getIntent().getExtras();

        facade = Facade.getInstance(this);

        error = new ShowMessage();

        try {
            facade.setDecimo(bundle.getString("numero"), bundle.getString("serie"),
                    bundle.getString("fraccion"), bundle.getString("idRaffle"));

            tv_numero.setText(facade.getDecimo().getNumero());
            tv_euros.setText(facade.getDecimo().getEuros());
            tv_fraccion.setText(facade.getDecimo().getFraccion());
            tv_serie.setText(facade.getDecimo().getSerie());
            tv_fecha.setText(facade.getDecimo().getFecha());

            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.
                    Builder().permitNetwork().build());
        } catch (IOException e) {
            error.setMessage(getResources().getString(R.string.msgErrorInternet)
                    , getResources().getString(R.string.msgError), null);
            error.show(getFragmentManager(), "errorMsg");
        } catch (IncorrectDataException e) {
            error.setMessage(getResources().getString(R.string.msgErrorData)
                    , getResources().getString(R.string.msgError), null);
            error.show(getFragmentManager(), "errorMsg");
        } catch (JSONException e) {
            error.setMessage(getResources().getString(R.string.msgErrorRaffleOut)
                    , getResources().getString(R.string.msgError), null);
            error.show(getFragmentManager(), "errorMsg");
        }
    }

    public void onClickBtn_keep(View view) {

        facade.addDecimo(facade.getDecimo());

        tv_fecha.setText("");
        tv_fraccion.setText("");
        tv_serie.setText("");
        tv_numero.setText("");
        tv_euros.setText("");

        finish();

        Toast.makeText(this, getResources().getString(R.string.decimoSaved), Toast.LENGTH_SHORT).show();

    }

    public void onClickBtn_check(View view) {

        ProgressDialog pDialog;
        pDialog = new ProgressDialog(VisualExtractActivity.this);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setMessage(getResources().getString(R.string.checking));
        pDialog.setCancelable(false);
        pDialog.setMax(100);

        AsyncTaskDialogCheck asyncTaskDialogCheck = new AsyncTaskDialogCheck(this, pDialog, facade.getDecimo(), null);
        asyncTaskDialogCheck.execute();

    }


}
