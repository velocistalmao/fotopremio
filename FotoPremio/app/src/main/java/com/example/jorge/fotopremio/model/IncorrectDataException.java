package com.example.jorge.fotopremio.model;

/**
 * Excepción propia, dato mal introducidos.
 */
public class IncorrectDataException extends Exception {

    public IncorrectDataException(String message) {
        super(message);
    }

}
