package com.example.jorge.fotopremio.base;

import android.os.StrictMode;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Esta clase se encarga de establecer una conexion a través de una url.
 */
public class ConnectionServer {


    /**
     * Contrusctor por defecto.
     */
    public ConnectionServer() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.
                Builder().permitNetwork().build());
    }


    /**
     * Establece una conexion con una url determinada.
     *
     * @param url url donde donde nos conectamos
     * @return Entidad
     * @throws IOException
     */
    public String connect(String url) throws IOException {

        DefaultHttpClient httpClient;
        HttpEntity httpEntity;
        HttpResponse httpResponse;
        HttpGet httpGet;

        httpClient = new DefaultHttpClient();
        httpGet = new HttpGet(url);
        httpResponse = httpClient.execute(httpGet);
        httpEntity = httpResponse.getEntity();

        return EntityUtils.toString(httpEntity);

    }

}
