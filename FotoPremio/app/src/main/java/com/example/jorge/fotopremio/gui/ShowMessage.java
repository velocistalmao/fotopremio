package com.example.jorge.fotopremio.gui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Clase que muestra un mensage pasado.
 */
public class ShowMessage extends DialogFragment {

    private String message;

    private String title;

    private String prize;

    public ShowMessage() {
    }

    public void setMessage(String message, String title, String prize) {
        this.message = message;
        this.title = title;
        this.prize = prize;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(title)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        if (prize != null)
            builder.setMessage(message + " " + prize + "€");
        else
            builder.setMessage(message);

        return builder.create();
    }

}
