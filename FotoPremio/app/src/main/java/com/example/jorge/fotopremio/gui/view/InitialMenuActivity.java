package com.example.jorge.fotopremio.gui.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.jorge.fotopremio.R;


/**
 * Clase que maneja al actividad activity_main.
 */
public class InitialMenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickBtn_camera(View view) {
        startActivity(new Intent(InitialMenuActivity.this, StartCameraActivity.class));
    }

    public void onClickBtn_manual(View view) {
        startActivity(new Intent(InitialMenuActivity.this, ManualActivity.class));
    }

    public void onClickBtn_list(View view) {
        startActivity(new Intent(InitialMenuActivity.this, LottoListActivity.class));
    }


}
