package com.example.jorge.fotopremio.gui.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.jorge.fotopremio.R;
import com.example.jorge.fotopremio.facade.Facade;
import com.example.jorge.fotopremio.gui.ItemDecimoAdapter;
import com.example.jorge.fotopremio.gui.ShowMessage;
import com.example.jorge.fotopremio.model.Decimo;

import java.util.List;

/**
 * Clase que visualiza la lista de décimos almacenados.
 */
public class LottoListActivity extends Activity {

    ListView lottoList;
    ShowMessage error;
    private List<Decimo> decimos;
    private Facade facade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        facade = Facade.getInstance(this);

        error = new ShowMessage();

        // getActionBar().setDisplayHomeAsUpEnabled(true);

        fillList();

        registerForContextMenu(lottoList);

    }

    /**
     *
     */
    private void fillList() {
        lottoList = (ListView) findViewById(R.id.listView_lotto1);

        decimos = facade.getDecimoList();

        ItemDecimoAdapter adapter = new ItemDecimoAdapter(this, decimos);

        lottoList.setAdapter(adapter);

        lottoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(LottoListActivity.this, DecimoDetailActivity.class);
                i.putExtra("numero", decimos.get(position).getNumero());
                i.putExtra("serie", decimos.get(position).getSerie());
                i.putExtra("fraccion", decimos.get(position).getFraccion());
                i.putExtra("euros", decimos.get(position).getEuros());
                i.putExtra("fecha", decimos.get(position).getFecha());
                startActivity(i);
            }

        });
        lottoList.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_list, menu);

       /* SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
        (SearchView) menu.findItem(R.id.searchaction).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));*/


        return true;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.deleteList) {

            facade.deleteAll();

            fillList();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getMenuInflater().inflate(R.menu.menu_options_listview, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        int id = item.getItemId();
        if (id == R.id.OptionDelete) {
            facade.deleteDecimo(decimos.get(info.position));
            fillList();
            return true;
        }

        return super.onContextItemSelected(item);

    }

}
